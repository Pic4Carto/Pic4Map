# Pic4Map

## License

Copyright 2017 Adrien PAVIE

See [LICENSE](LICENSE.txt) for complete AGPL3 license.

Pic4Map is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

Pic4Map is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with Pic4Map. If not, see http://www.gnu.org/licenses/.


### Libraries

* [Material UI Webpack](https://github.com/callemall/material-ui-webpack-example) - MIT License
